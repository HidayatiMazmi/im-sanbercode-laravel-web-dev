<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){
        $namaDepan = $request->input('first_name');
        $namaBelakang = $request->input('last_name');
        $gender = $request->input('gender');
        $nationality = $request->input('nationality');
        $language = $request->input('language');
        $bio = $request->input('bio');
        $data = [
            "namaDepan" => $namaDepan, 
            "namaBelakang" => $namaBelakang,
            "gender" => $gender,
            "nationality" => $nationality,
            "language" => $language,
            "bio" => $bio
        ];

        return view('home',$data);
    }
}
