@extends('layouts.master')

@section('title')
Cast
@endsection

@push('scripts')
<script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<a href="{{ route('create_cast') }}" class="btn btn-primary btn-sm my-3">Create Cast</a>

<div class="card">
    <table id="cast" class="table table-bordered table-striped">
        <thead>
        <tr>
        <th>#</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($cast as $key => $row)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $row->nama }}</td>
                <td>{{ $row->umur }}</td>
                <td>{{ $row->bio }}</td>
                <td>
                    <form action="/cast/{{ $row->id }}" method="post">
                        @method('delete')
                        @csrf
                        <a href="/cast/{{ $row->id }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{ $row->id }}/edit" class="btn btn-warning btn-sm my-3">Edit Cast</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td colspan="5" align="center">
                <p>Data Cast Kosong</p>
            </td>
        </tr>
        @endforelse
        </tbody>
    </table>
</div>
@endsection