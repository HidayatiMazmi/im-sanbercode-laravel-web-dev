@extends('layouts.master')

@section('title')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ $cast->nama }}">
    </div>
    @error('nama') 
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ $cast->umur }}">
    </div>
    @error('umur') 
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea type="text" class="form-control @error('bio') is-invalid @enderror" id="bio" name="bio">{{ $cast->bio }}</textarea>
    </div>
    @error('bio') 
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection