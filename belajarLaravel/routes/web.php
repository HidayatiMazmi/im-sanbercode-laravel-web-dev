<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DatatableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
// Route::get('/register', function () {
//     return view('register');
// });

Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/home', [HomeController::class, 'index'])->name('home');

Route::get('/table', [TableController::class, 'index'])->name('table');
Route::get('/datatable', [DatatableController::class, 'index'])->name('datatable');

Route::get('/cast', [CastController::class, 'index'])->name('cast');
Route::post('/cast', [CastController::class, 'store'])->name('store_cast');
Route::get('/cast/create', [CastController::class, 'create'])->name('create_cast');
Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('show_cast');
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('edit_cast');
Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('update_cast');
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('delete_cast');
