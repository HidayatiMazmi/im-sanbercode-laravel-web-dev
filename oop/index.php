<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>";
echo "Legs : ".$sheep->legs."<br>";
echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br>";
echo "Legs : ".$kodok->legs."<br>";
echo "Cold Blooded : ".$kodok->cold_blooded."<br>";
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br>";
echo "Legs : ".$sungokong->legs."<br>";
echo "Cold Blooded : ".$sungokong->cold_blooded."<br>";
$sungokong->yell(); // "Auooo"

?>